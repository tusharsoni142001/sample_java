import java.util.Scanner;
import java.io.*;
public class ReadFile {
    public static void main(String[] args) {
        try{
            File myFile=new File("sample.txt");
            Scanner scan=new Scanner(myFile);
            while(scan.hasNextLine())
            {
                System.out.println(scan.nextLine());
            }
            scan.close();
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }

    }
}
