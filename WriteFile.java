import java.io.*;

public class WriteFile {
    static void writeFile(File fs) {
        try {
            FileWriter fw = new FileWriter(fs);
            fw.write("Hello Tushar!!");
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void main(String args[]) {
        File fs = new File("sample.txt");
        try {
            if (fs.createNewFile()) {
                System.out.println("File create with name: " + fs.getName());
                writeFile(fs);
            } else {
                System.out.println("File already exist");
                writeFile(fs);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
